# CI image for React Native Android + Fastlane
# Based from https://github.com/bitrise-docker/android/blob/master/Dockerfile
FROM openjdk:8-jdk

ENV ANDROID_HOME /opt/android-sdk-linux
ENV ANDROID_SDK "26"
ENV ANDROID_SDK_TOOLS "4333796"
ENV ANDROID_BUILD_TOOLS "27.0.3"

# --- Download Android SDK tools into $ANDROID_HOME ---
RUN cd /opt \
  && wget -q https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip -O android-sdk-tools.zip \
  && unzip -q android-sdk-tools.zip -d ${ANDROID_HOME} \
  && rm android-sdk-tools.zip

ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools

# --- Install Android SDKs and other build packages ---
# Accept licenses before installing components
RUN yes | sdkmanager --licenses

# Platform tools
RUN sdkmanager "emulator" "tools" "platform-tools"

# Please keep all sections in descending order!
RUN yes | sdkmanager \
  "platforms;android-${ANDROID_SDK}" \
  "build-tools;${ANDROID_BUILD_TOOLS}" \
  "system-images;android-${ANDROID_SDK};google_apis;x86" \
  "extras;android;m2repository" \
  "extras;google;m2repository" \
  "extras;google;google_play_services"

# --- Install Ruby with rbenv ---
ENV RUBY_VERSION "2.5.1"

RUN apt-get update \
  && apt-get install -y --no-install-recommends autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev \
  && echo "gem: --no-document" > ~/.gemrc \
  && git clone https://github.com/rbenv/rbenv.git ~/.rbenv \
  && git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build \
  && rm -rf /var/lib/apt/lists/*

ENV PATH "/root/.rbenv/shims:/root/.rbenv/bin:${PATH}"
RUN rbenv install ${RUBY_VERSION} \
  && rbenv global ${RUBY_VERSION}

# --- Install Fastlane ---
RUN gem install fastlane --no-document \
  && fastlane --version

# --- Install nodejs with nodenv ---
ENV NODE_VERSION "10.9.0"

ENV PATH "/root/.nodenv/shims:/root/.nodenv/bin:${PATH}"
RUN git clone https://github.com/nodenv/nodenv.git ~/.nodenv \
  && git clone https://github.com/nodenv/node-build.git ~/.nodenv/plugins/node-build

RUN nodenv install ${NODE_VERSION} \
  && nodenv global ${NODE_VERSION}

# --- Install yarn ---
RUN apt-get update \
  && apt-get install -y --no-install-recommends apt-transport-https \
  && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
  && apt-get update && apt-get install -y --no-install-recommends yarn \
  && rm -rf /var/lib/apt/lists/*

# --- Install other dependencies ---
RUN  apt-get update && apt-get install -y --no-install-recommends \ 
  awscli \
  imagemagick \
  librsvg2-bin \
  libcurl4 \
  curl \
  libcurl4-openssl-dev \
  && rm -rf /var/lib/apt/lists/*
