build:
	@docker build -t hashlabs/react-native-gitlab-ci:latest .

push:
	@docker push hashlabs/react-native-gitlab-ci:latest
