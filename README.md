# docker-react-native-gitlab-ci

This Docker image has everything you need to test and build React Native applications with Gitlab CI

This image has installed:

- Android SDK
- Node.js
- Ruby
- Fastlane
- AWS CLI
